'use strict'

class Config {
  constructor (setting) {
    this.cfg = setting || {};
  }

  get (key, def) {
    return (key in this.cfg)
      ? this.cfg[key] : def || null;
  }

  set (key, value) {
    this.cfg[key] = value;
  }

  exists (key) {
    return (key in this.cfg);
  }

  config() {
    return this.cfg;
  }
}
module.exports = Config;

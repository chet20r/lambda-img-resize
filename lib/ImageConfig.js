'use strict'

const Config = require('./Config');

class ImageConfig extends Config {

  constructor (settings) {
    super(settings);
  }

  getSwatch () {
    return super.get(cfg.swatch, {
      "width" : 70,
      "height" : 70,
      "folder" : "swatch"
    });
  }

  getThumbnail () {
    return super.get(cfg.thumbnail, {
      "width" : 280,
      "height" : 280,
      "folder" : "thumbnail"
    });
  }

  getMedium () {
    return super.get(cfg.medium, {
      "width" : 480,
      "height" : 480,
      "folder" : "medium"
    });
  }

  getLarge () {
    return super.get(cfg.thumbnail, {
      "width" : 1200,
      "height" : 1200,
      "folder" : "large"
    });
  }
}

module.exports = ImageConfig;

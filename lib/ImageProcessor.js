'use strict'

const AWS = require('aws-sdk');
const S3 = new AWS.S3();
const path = require('path');

const Config = require('./ImageConfig');

let gm = require('gm').subClass({
  imageMagick : true
});

class ImageProcessor {
  constructor (imageConfig, s3Event) {
    this.config = imageConfig;
    this.s3Event = s3Event;
  }

  process (callback) {
    if (this.s3Event.getBucketName === this.config.destBucket) {
      console.log("Source and destination buckets cannot be same");
      return;
    }

    if (this.s3Event.isPutEvent()) {
      return Promise.resolve(this._download());
    } else if (this.s3Event.isDeleteEvent()) {
      return Promise.resolve(this._cleanupImages());
    }

  }

  _download () {
    console.log("downloading image "+this.s3Event.getObjectKey());
    console.log("source bucket "+ this.s3Event.getBucketName());
    S3.getObject({
        Bucket: this.s3Event.getBucketName(),
        Key: this.s3Event.getObjectKey()
    }, (err, data) => {
      if (err) throw err;
      this._optimize(data);
    });
  }

  _optimize (data) {
    console.log("optimizing");
    gm(data.Body)
      .antialias(true)
      .density(300)
      .toBuffer('JPG', (err, buffer) => {
        if(err) throw err;
        this._resizeAndUpload(buffer);
      });
  }

  _resizeAndUpload (buff) {
    for(let size in this.config.resize) {
      console.log("processing "+size);
      let width = this.config.resize[size].width;
      let height = this.config.resize[size].height;
      gm(buff)
        .resize(width, height)
        .setFormat('jpeg')
        .toBuffer( (err, buffer) => {
          if (err) throw err;
          this._upload({
            buffer : buffer,
            folder : this.config.resize[size].folder
          });
        });
      }
  }

  _upload (data) {
    let destObjectName = data.folder + "/" +
      path.basename(this.s3Event.getObjectKey())
        .slice (0, -4) + ".jpg";
    console.log("Uploading image "+destObjectName);
    S3.putObject({
        Bucket: this.config.destBucket,
        Key: destObjectName,
        Body: data.buffer,
        ContentType: 'JPG'
    }, (err, data) => {
      if(err) throw err;
      return data;
    });
  }

  _cleanupImages () {
    for(let size in this.config.resize) {
      let folderName = this.config.resize[size].folder;
      let params = {
        Bucket : this.config.destBucket,
        Key : folderName + "/" + this.s3Event.getObjectKey()
      }
      S3.deleteObject (params, (err, data) => {
        if (err) {
          throw err;
        }
        return data;
      });
    }
  }
}

module.exports = ImageProcessor;

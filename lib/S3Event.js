'use strict'

class S3Event {
  constructor (event) {
    this.event = event.Records[0];
    this.s3 = this.event.s3;
  }

  getEventName () {
    return this.event.eventName;
  }

  getBucketName () {
    return this.s3.bucket.name;
  }

  getObjectKey () {
    return decodeURIComponent(this.s3.object.key.replace(/\+/g, " "));
  }

  getObjectSize () {
    return this.s3.object.size;
  }

  getArn () {
    return this.s3.bucket.arn;
  }
}

module.exports = S3Event;

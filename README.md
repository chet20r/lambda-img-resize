# Image Resize Function #

### What is this repository for? ###

* Resize the image that's uploaded to a s3 bucket as per the resize configuration in config.json and then upload the resized images to the target s3 bucket specified in the config.
* Version 1.0.0

### How do I get set up? ###

* clone the repository `git clone https://bitbucket.org/chet20r/lambda-img-resize.git` and then `npm install`
* config.json is the configuration file that defines the resizes and the destination S3 bucket
* aws-sdk, graphics magic
* Zip the source and upload it to a aws lambda function and associate the lambda function with your source s3 bucket.

### TODO ###
* Handle deletes. When an image is deleted from the source bucket, delete all the resized images from the target s3 bucket
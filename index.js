'use strict'

//dependencies
const path = require('path');
const fs = require('fs');

//local dependencies
const ImageConfig = require('./lib/ImageConfig');
const ImageProcessor = require('./lib/ImageProcessor');
const S3Event = require('./lib/S3Event');

const CONFIG_FILE = path.resolve(__dirname, "config.json");

// read config file
function readFile (filePath) {
  return new Promise( (resolve, reject) => {
    fs.readFile(filePath, 'utf8', (err, data) => {
      if (err) reject(err);
      resolve(data);
    });
  });
};

// aws lambda handler
exports.handler = function (event, context) {
  let s3Event = new S3Event(event);
  // read the config file
  readFile(CONFIG_FILE).then( (config) => {
    return new Promise( (resolve, reject) => {
      if (!config) throw "config not found / empty";
      resolve(new ImageConfig(JSON.parse(config)).config());
    });
  })
  // then run through the image processor
  .then ( (imageConfig) => {
    new ImageProcessor(imageConfig, s3Event).process();
  })
  .then( (data) => {
    console.log("Processing is finished: "+data);
  })
  .catch( (err) => {
    console.log(err);
    throw err;
  });
}
